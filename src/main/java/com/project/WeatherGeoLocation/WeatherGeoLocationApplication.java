package com.project.WeatherGeoLocation;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.WeatherGeoLocation.model.City;
import com.project.WeatherGeoLocation.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@SpringBootApplication
@EnableSwagger2
public class WeatherGeoLocationApplication {

    private final CityService cityService;

    @Autowired
    public WeatherGeoLocationApplication(CityService cityService) {
        this.cityService = cityService;
    }

    public static void main(String[] args) {
        SpringApplication.run(WeatherGeoLocationApplication.class, args);
    }


    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.HireRight")).build();
    }


    @Bean
    CommandLineRunner runner(CityService cityService) {
        return args -> {
            ObjectMapper mapper = new ObjectMapper();
            TypeReference<List<City>> typeReference = new TypeReference<List<City>>() {
            };
            InputStream inputStream = TypeReference.class.getResourceAsStream("/json/cities.json");
            try {
                List<City> cities = mapper.readValue(inputStream, typeReference);
                cityService.saveCity(cities);
                System.out.println("Cities JSON data Saved in dB");
            } catch (IOException e) {
                System.out.println("Unable to save cities: " + e.getMessage());
            }
        };
    }
}

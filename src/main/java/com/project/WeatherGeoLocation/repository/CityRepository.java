package com.project.WeatherGeoLocation.repository;

import com.project.WeatherGeoLocation.model.City;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CityRepository extends JpaRepository<City, Long> {
    City findByZip(Long id);
}

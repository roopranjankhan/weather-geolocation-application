package com.project.WeatherGeoLocation.model;

import javax.persistence.Embeddable;

@Embeddable
public class Coordinates {

    private Double lon;

    private Double lat;

    public Double getLat() {
        return lat;
    }

    public Coordinates() {
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "lat=" + lat +
                ", lon=" + lon +
                '}';
    }
}

package com.project.WeatherGeoLocation.model;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class City {
    private String base;

    private Integer visibility;

    @Id
    private Long id;

    private String name;

    private String cod;

    private String timezone;

    private Long zip;

    public City() {
    }

    @Embedded
    private Coordinates coordinates;

    @Embedded
    private Weather weather;

    public String getBase() {
        return base;
    }

    public void setBase(String base) {
        this.base = base;
    }

    public Long getZip() {
        return zip;
    }

    public void setZip(Long zip) {
        this.zip = zip;
    }

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public Weather getWeather() {
        return weather;
    }

    public void setWeather(Weather weather) {
        this.weather = weather;
    }

    public Integer getVisibility() {
        return visibility;
    }

    public void setVisibility(Integer visibility) {
        this.visibility = visibility;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }

    @Override
    public String toString() {
        return "City{" +
                "base='" + base + '\'' +
                ", visibility=" + visibility +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", cod='" + cod + '\'' +
                ", timezone='" + timezone + '\'' +
                ", zip=" + zip +
                ", coordinates=" + coordinates +
                ", weather=" + weather +
                '}';
    }
}

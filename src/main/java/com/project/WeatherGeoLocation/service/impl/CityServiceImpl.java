package com.project.WeatherGeoLocation.service.impl;

import com.project.WeatherGeoLocation.model.City;
import com.project.WeatherGeoLocation.repository.CityRepository;
import com.project.WeatherGeoLocation.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CityServiceImpl implements CityService {
    private final CityRepository cityRepository;

    @Autowired
    public CityServiceImpl(CityRepository cityRepository) {
        this.cityRepository = cityRepository;
    }

    @Override
    public City findWeatherById(Long id) {
        return cityRepository.findOne(id);
    }

    @Override
    public Iterable<City> list() {
        return cityRepository.findAll();
    }

    @Override
    public City save(City city) {
        return cityRepository.save(city);
    }

    @Override
    public Iterable<City> saveCity(List<City> cities) {
        return cityRepository.save(cities);
    }

    @Override
    public City findLocationByZip(Long zip) {
        return cityRepository.findByZip(zip);
    }
}

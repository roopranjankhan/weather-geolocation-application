package com.project.WeatherGeoLocation.service;

import com.project.WeatherGeoLocation.model.City;

import java.util.List;

public interface CityService {
    public City findWeatherById(Long id);
    public City save(City city);
    public Iterable<City> saveCity(List<City> cities);
    public Iterable<City> list();
    public City findLocationByZip(Long zip);
}

package com.project.WeatherGeoLocation.controller;

import com.project.WeatherGeoLocation.model.City;
import com.project.WeatherGeoLocation.service.CityService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class CityController {
    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    Logger logger = LoggerFactory.getLogger(CityController.class);

    @GetMapping("/cities/list")
    public Iterable<City> list() {
        Iterable<City> city = cityService.list();
        ResponseEntity responseEntityOk = new ResponseEntity(city, HttpStatus.OK);
        logger.info("city list found: " + responseEntityOk);
        return city;
    }

    @GetMapping("cities/{id}")
    public ResponseEntity<City> findWeatherById(@PathVariable("id") Long id) {

        logger.info("id is: " + id);
        City city = cityService.findWeatherById(id);
        ResponseEntity responseEntityOk = new ResponseEntity(city, HttpStatus.OK);
        ResponseEntity responseEntityNotFound = new ResponseEntity(city, HttpStatus.OK);

        try {
            logger.info("id is: " + responseEntityOk);
            return responseEntityOk;
        } catch (Exception e) {
            logger.info("id not found " + responseEntityNotFound);
            return responseEntityNotFound;
        }
    }

    @PostMapping(value = "/cities")
    public ResponseEntity<City> save(@RequestBody City city) {
        logger.info("city is: " + city);
        City newCity = cityService.save(city);
        ResponseEntity responseEntityOk = new ResponseEntity(newCity, HttpStatus.OK);
        logger.info("city saved: " + responseEntityOk);
        return responseEntityOk;
    }

    @GetMapping(value = "/cities/ZIP/{zip}")
    public ResponseEntity<City> findLocationByZip(@PathVariable("zip") Long zip) {
        logger.info("Zip code is: " + zip);
        City city = cityService.findLocationByZip(zip);
        ResponseEntity responseEntityOk = new ResponseEntity(city, HttpStatus.OK);
        ResponseEntity responseEntityNotFound = new ResponseEntity(city, HttpStatus.NOT_FOUND);
        if (city != null) {
            logger.info("Zip code is: " + responseEntityOk);
            return responseEntityOk;
        } else {
            logger.info("Zip code not found " + responseEntityNotFound);
            return responseEntityNotFound;
        }
    }
}

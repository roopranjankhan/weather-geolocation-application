package ServiceLayerTest;

import com.project.WeatherGeoLocation.model.City;
import com.project.WeatherGeoLocation.repository.CityRepository;
import com.project.WeatherGeoLocation.service.impl.CityServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {

    @Mock
    CityRepository cityRepository;


    private CityServiceImpl cityService = new CityServiceImpl(cityRepository);

    @Before
    public void beforeEachTest() {
        City city = new City();
        city.setId(1l);
        city.setName("Mustamae");
        city.setCod("+372 56");
        city.setTimezone("UTC+2; UTC+3(summer)");
        city.setBase("camp");
        city.setVisibility(678);
        city.setZip(3456l);
        cityRepository.save(city);
    }

    @Test
    public void findWeatherByIdTest() {
        City city = cityService.findWeatherById(1l);
        Assert.assertNotNull(city.getId());
    }

    @Test
    public void findLocationByZipTest() {
        City city = cityService.findLocationByZip(3456l);
        Assert.assertNotNull(city);
    }

    @Test
    public void saveTest() {
        City city = new City();
        city.setZip(3456l);
        city.setVisibility(9000);
        city.setBase("stations");
        city.setName("test city");
        city.setCod("+372 78");
        city.setTimezone("UTC+2");
        City city1 = cityService.save(city);
        Assert.assertNotNull(city1.getId());
    }
}

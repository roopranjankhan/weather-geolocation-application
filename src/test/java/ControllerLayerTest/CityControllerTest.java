package ControllerLayerTest;

import com.project.WeatherGeoLocation.controller.CityController;
import com.project.WeatherGeoLocation.model.City;
import com.project.WeatherGeoLocation.service.CityService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CityControllerTest {
    @InjectMocks
    CityController cityController;

    @Mock
    CityService cityService;

    @Test
    public void findWeatherByIdTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        City city = new City();
        city.setName("Mustamae");
        city.setVisibility(2345);
        city.setTimezone("UTC+2; UTC+3");
        city.setBase("camp");
        city.setCod("+372 56");
        when(cityService.findWeatherById(anyLong())).thenReturn(city);

        ResponseEntity<City> responseEntity = cityController.findWeatherById(1l);


        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void findLocationByZipTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        City city = new City();
        city.setName("Lasnamae");
        city.setVisibility(23456);
        city.setTimezone("UTC+2; UTC+3");
        city.setBase("base");
        city.setCod("+372 56");
        when(cityService.findLocationByZip(anyLong())).thenReturn(city);

        ResponseEntity<City> responseEntity = cityController.findLocationByZip(61501l);


        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
    }

    @Test
    public void saveTest() {
        MockHttpServletRequest request = new MockHttpServletRequest();
        RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
        City city = new City();
        city.setName("Pirita");
        city.setVisibility(23456);
        city.setTimezone("UTC+2; UTC+3");
        city.setBase("base");
        city.setCod("+372 56");
        when(cityService.save(any(City.class))).thenReturn(city);

        ResponseEntity<City> responseEntity = cityController.save(city);


        Assert.assertEquals(200, responseEntity.getStatusCodeValue());
    }
}
